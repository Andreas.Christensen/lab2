package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	ArrayList<FridgeItem> fridge = new ArrayList<>();
	ArrayList<FridgeItem> expiredItems = new ArrayList<>();

	@Override
	public int nItemsInFridge() {
		return fridge.size();
	}

	@Override
	public int totalSize() {
		return 20;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (nItemsInFridge() < totalSize()) {
			fridge.add(item);
			return true;
		}
		return false;
	}

	@Override
	public void takeOut(FridgeItem item){
		if (nItemsInFridge() < 1) {
			throw new NoSuchElementException();
		}
		fridge.remove(item);
	}

	@Override
	public void emptyFridge() {
		fridge.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		for(FridgeItem item : fridge) {
			if (item.hasExpired()) {
				expiredItems.add(item);
			}
		}
		fridge.removeAll(expiredItems);
		return expiredItems;
	}

}
